(** Client_state module, variables and data for the client *)

(** the current game_state *)
val gs : Game_state.game_state option ref

(** address and port of the server *)
val port : int ref
val addr : string ref

(** set this to true when cahnge address/port *)
val server_changed : bool ref

(** game mode *)
type game_mode = Solo | Multi

(** game_mode solo by default *)
val game_mode : game_mode ref

(** optional json file to load *)
val filename : string ref

(** connect to the server and ask the list of running games *)
val get_list_of_games : unit ->
                        (string * string * string * int * int * bool) list
